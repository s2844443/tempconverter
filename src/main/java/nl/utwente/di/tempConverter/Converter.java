package nl.utwente.di.tempConverter;

public class Converter {
    double celsiusToFahrenheit(double celsius) {
        return ((celsius * 9/5) + 32);
    }
}